import test from "ava";
import moment from "moment";
import { JSDOM } from "jsdom";
import { LoremIpsum } from "lorem-ipsum";

import { ActivityClock } from "../src/index.js";

/******************** PARAMS ********************/

const lorem = new LoremIpsum();
let data = [...Array(10).keys()].map(i => { 
    let start = moment().subtract(Math.floor(Math.random() * 300),"day");
    return {
        date_start: start.utc(), 
        date_end: moment(start).add(Math.floor(Math.random() * 3),"day").utc(), 
        event_title: `Event ${i}`
    }
});

/******************** EMPTY VARIABLES ********************/

let ac = new ActivityClock(null,data);

test("init", t => {
    const dom = new JSDOM("<figure></figure>");
    global.document = dom.window.document;
    let figure = global.document.querySelector("figure");
    ac.render(figure);
    t.true(figure.innerHTML !== "");
});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    ac.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});*/

/******************** DECLARED PARAMS ********************/

/*let testWidth = 300;
let testHeight = 500;
let testData = [
    {label: "xyz", id: 1, value: 1},
    {label: "abc", id: 2, value: 4}
];

// initialize
let act = new ActivityCalendar(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(act.height === testHeight);
    t.true(act.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    act.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
