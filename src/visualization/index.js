import { max } from "d3-array";
import { scaleQuantize } from "d3-scale";
import { select } from "d3-selection";
import { ActivityLegend, ChartLabel, RadialGrid, simplifyLargeNumber } from "@lgv/visualization-chart";

import { ClockLayout as CL } from "../layout/clock.js";
import { configuration, configurationColor, configurationLayout } from "../configuration.js";

/**
 * ActivityClock is a time series visualization.
 * @param {array} color - strings of css color value
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {string} label - unique prefix to add to element identifers
 * @param {string} name - name of visualization to use as element id
 * @param {integer} unit - smallest atom size if artboard was grided
 * @param {integer} width - artboard width
 */
class ActivityClock extends RadialGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, ClockLayout=null, color=configurationColor, unit=16, label=configuration.branding, name=configuration.name) {
        
        // initialize inheritance
        super(data, width, height, ClockLayout ? ClockLayout : new CL(data,{height:height,unit:unit,width:width}), label, name);

        // update self
        this.arc = null;
        this.cell = null;
        this.classArc = `${label}-arc`;
        this.classCell = `${label}-cell`;
        this.classLabel = `${label}-label`;
        this.classLabelArc = `${label}-label-arc`;
        this.classLabelRing = `${label}-label-ring`;
        this.classRingGroup = `${label}-group-ring`;
        this.color = color;
        this.label = null;
        this.labelArc = null;
        this.labelRing = null;
        this.Legend = new ActivityLegend(this.legend,label,name);

    }

    /**
     * Generate color scale for activity value.
     * @returns A d3 scale function.
     */
    get colorScale() {
        return scaleQuantize()
            .domain([0, max(this.data, d => d.value)])
            .range(this.color);
    }

    /**
     * Generate legend data.
     * @returns A list of objects with key/value pairs of label,swatch,value
     */
    get legend() {
        return [0].concat(this.colorScale.thresholds()).map((d,i) => ({label:simplifyLargeNumber(d),swatch:this.colorScale.range()[i],value:d}));
    }

    /**
     * Position and minimally style clock cells in SVG dom element.
     */
    configureCells() {
        this.cell
            .attr("class", this.classCell)
            .attr("d", d => d.path)
            .attr("fill", "transparent")
            .attr("pointer-events", "none")
            .attr("stroke", "currentColor");
    }

    /**
     * Position and minimally style arc labels in SVG dom element.
     */
    configureArcLabels() {
        this.labelArc
            .attr("class", this.classLabelArc)
            .attr("data-arc", d => d.arc)
            .attr("data-angle-orthant", d => this.orthant(d.degree))
            .attr("x", d => d.centroid[0])
            .attr("y", d => d.centroid[1])
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .attr("transform", d => `translate(0,${this.Data.arcLabelYOffset(this.orthant(d.degree))})`)
            .text(d => d.arc);
    }

    /**
     * Position and minimally style arcs in SVG dom element.
     */
    configureArcs() {
        this.arc
            .attr("class", this.classArc)
            .attr("data-arc", d => d.arc)
            .attr("data-ring", d => d.ring)
            .attr("d", d => d.path)
            .attr("fill", d => this.colorScale(this.Data.extractValue(d)));
    }

    /**
     * Configure arc click event.
     */
    configureArcEventClick() {
        this.arc.on("click", (e,d) => this.configureEvent("arc-click",d,e));
    }

    /**
     * Configure arc mouseout event.
     */
    configureArcEventMouseout() {
        this.arc
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classArc);

                // send event to parent
                this.artboard.dispatch("arc-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Configure arc mouseover event.
     */
    configureArcEventMouseover() {
        this.arc
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classArc} active`);

                // send event to parent
                this.configureEvent("arc-mouseover",d,e);

            });
    }

    /**
     * Configure events on arcs in SVG dom element.
     */
    configureArcEvents() {
        this.configureArcEventClick();
        this.configureArcEventMouseout();
        this.configureArcEventMouseover();
    }

    /**
     * Position and minimally style arc value labels in SVG dom element.
     */
    configureLabels() {
        this.label
            .attr("class", this.classLabel)
            .attr("data-angle-orthant", d => this.orthant(d.degree))
            .attr("x", d => d.centroid[0])
            .attr("y", d => d.centroid[1])
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .attr("transform", d => `translate(0,${this.Data.arcLabelYOffset(this.orthant(d.degree))})`)
            .text(d => this.Data.extractValueAsLabel(d));
    }

    /**
     * Position and minimally style ring labels in SVG dom element.
     */
    configureRingLabels() {
        this.labelRing
            .attr("class", this.classLabelRing)
            .attr("data-ring", d => d)
            .attr("x", 0)
            .attr("y", d => -this.Data.ringLabelScale(d))
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .text(d => d);
    }

    /**
     * Generate labels for arcs in SVG elements.
     * @param {selection} selection - d3.js selection
     * @returns A d3.js selection.
     */
    generateArcLabels(selection) {
        return selection
            .selectAll(`.${this.classLabelArc}`)
            .data(this.Data.constructArcGrid("label",this.Data.rings.length).flat())
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate arcs in SVG elements.
     * @param {selection} selection - d3.js selection
     * @returns A d3.js selection.
     */
    generateArcs(selection) {
        return selection
            .selectAll(`.${this.classArc}`)
            .data(this.data)
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate concentric rings of arc shapes in SVG element.
     * @param {selection} selection - d3.js selection
     * @returns A d3.js selection.
     */
    generateCells(selection) {
        return selection
            .selectAll(`.${this.classCell}`)
            .data(this.Data.rings.map((r,i) => this.Data.constructArcGrid(r,i)).flat())
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     * @param {selection} artboard - d3.js SVG selection
     * @param {selection} selection - d3.js SVG selection
     */
    generateChart(selection, artboard) {

        // determine dom container
        let container = selection || this.content;

        // initialize labeling
        this.Label = new ChartLabel(artboard || this.artboard, this.Data);

        // generate arcs
        this.arc = this.generateArcs(container);
        this.configureArcs();
        this.configureArcEvents();

        // generate each cell
        this.cell = this.generateCells(container);
        this.configureCells();

        // generate ring labels
        this.labelRing = this.generateRingLabels(container);
        this.configureRingLabels();

        // generate arc labels
        this.labelArc = this.generateArcLabels(container);
        this.configureArcLabels();

        // generate labels
        this.label = this.generateLabels(container);
        this.configureLabels();

    }

    /**
     * Generate arc value labels in SVG element.
     * @param {selection} selection - d3.js SVG selection
     * * @returns A d3.js selection.
     */
    generateLabels(selection) {
        return selection
            .selectAll(`.${this.classLabel}`)
            .data(this.data)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate labels for rings in SVG elements.
     * @param {selection} selection - d3.js selection
     * @returns A d3.js selection.
     */
    generateRingLabels(selection) {
        return selection
            .selectAll(`.${this.classLabelRing}`)
            .data(this.Data.rings)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

}

export { ActivityClock };
export default ActivityClock;