import { configuration as config, configurationLayout as configLayout } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: process.env.LGV_BRANDING || config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationColor = ["#C2C2C2", "#A6A6A6", "#8A8A8A", "#6D6D6D", "#515151"];

const configurationLayout = {
    height: process.env.LGV_HEIGHT || configLayout.height,
    width: process.env.LGV_WIDTH || configLayout.width
}

export { configuration, configurationColor, configurationLayout };
export default configuration;
