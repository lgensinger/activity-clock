import moment from "moment";

import { rollup, sum } from "d3-array";
import { scaleBand } from "d3-scale";
import { arc } from "d3-shape";

import { degreeToRadian, radianToDegree, TimeData } from "@lgv/visualization-chart";

/**
 * ClockLayout is a data-bound layout abstraction for a clock chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - key/value pairs needed for getters
 */
class ClockLayout extends TimeData {

    constructor(data, params) {

        // initialize inheritance
        super(data, {...params,...{timeFill:"hour"}});

        // update self
        this.data = this.update;

    }

    /**
     * Declare css Y offset value for arc label transform.
     * @returns A float representing the em value to offset in css translate transformation.
     */
    get arcLabelYOffset() {
        return d => [0.35,0.25,0.25,0.5,0.35,0.25,0.25,0.25].map(d => d * this.params.unit)[d-1];
    }

    /**
     * Declare arc labels.
     * @returns An array of string where each represents a concentric ring.
     */
    get arcs() {
        return [...Array(12).keys()].map(d => `${d+1}`);
    }

    /**
     * Determine arc time format using moment.js parsing.
     * @returns A string representing the moment.js parser variable.
     */
    get arcParse() {
        return "h";
    }

    /**
     * Determine the cell value in degrees.
     * @returns A float representing the value of a single cell.
     */
    get degree() {
        return 360 / this.arcs.length;
    }

    /**
     * Determine diameter of inner ring.
     * @returns A float representing the radius of the innermost ring.
     */
    get inner() {
        return this.radius * 0.5;
    }

    /**
     * Initial label parsing of data.
     * @returns An object of key/value pairs representing an individual arc.
     */
    get labelParse() {
        return d => ({
            arc: moment(d.timestamp).format(this.arcParse),
            ring: moment(d.timestamp).format(this.ringParse)
        });
    }

    /**
     * Determine diameter of outer ring.
     * @returns A float representing the radius of the outer ring.
     */
    get outer() {
        return this.radius;
    }

    /**
     * Determine radius.
     * @returns A float representing the radius of the clock.
     */
    get radius() {
        return Math.min(this.params.height,this.params.width) / 2;
    }

    /**
     * Determine ring width.
     * @returns A float representing the width of the ring.
     */
    get ring() {
        return this.ringScale.bandwidth();
    }

    /**
     * Determine ring grid to include ring labels and a ring for labeling.
     * @returns An array of strings each representing a ring.
     */
    get ringGrid() {
        return this.rings.concat(["label"]);
    }

    /**
     * Generate ring label scale for outer and inner most rings.
     * @returns A d3 scale function.
     */
    get ringLabelScale() {
        
        // configure position based on arc v text size
        let isArcTallerThanText = this.ring > this.params.unit;
        let insidePosition = isArcTallerThanText ? this.ring / 2 : this.params.unit;
        let outsidePosition = isArcTallerThanText ? this.ring * 2 : this.params.unit * 2;

        // establish scale
        return scaleBand()
           .domain(this.rings)
           .rangeRound([this.inner-insidePosition, this.outer+outsidePosition]);
           
    }

    /**
     * Determine ring radius.
     * @returns A float representing the outer radius of a ring.
     */
    get ringRadius() {
        return d => this.ringScale(this.ringGrid[d]);
    }

    /**
     * Declare ring labels.
     * @returns An array of string where each represents a concentric ring.
     */
    get rings() {
        return ["am", "pm"];
    }

    /**
     * Generate ring scale for concentric circles.
     * @returns A d3 scale function.
     */
    get ringScale() {
        return scaleBand()
           .domain(this.ringGrid)
           .rangeRound([this.inner, this.outer]);
    }

    /**
     * Determine ring time format using moment.js parsing.
     * @returns A string representing the moment.js parser variable.
     */
    get ringParse() {
        return "a";
    }

    /**
     * Rotate to align arcs to analog clock dial visually.
     */
    get rotation() {
        return 15;
    }

    /**
     * Determine value key to parse from data.
     * @returns A string representing the key representing the value for each object.
     */
    get valueParse() {
        return "value";
    }

    /**
     * Condition data for visualization requirements.
     * @param {object} data - usually array; mapped object of visualization values
     * @param {object} params - key/value pairs used for conditioning
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {

        // add datum for hours inside start/end
        let filled = Array.from(this.fillHours(data)).map(d => d[1]).flat();

        // parse by time
        let _data = filled
            .map(d => {
                let label = this.labelParse(d);
                return this.constructDatum(label.ring,this.rings.indexOf(label.ring),label.arc,this.arcs.indexOf(label.arc),d);
            });

        // aggregate values
        let aggregate = rollup(_data ? _data : [],
            v => sum(v, d => d.value),
            d => d.ring,
            d => d.arc
        );

        // unnest and simplify to 1d array
        // prune to required data
        // add unique id
        let flat = Array.from(aggregate, ([k,v]) => Array.from(v, ([kk,vv]) => ({ arc: kk, ring: k, value: vv })).flat() )
            .flat()
            .map((d,i) => { d.id = i; return d; });

        // format for render
        return flat.map(d => this.constructDatum(d.ring,this.rings.indexOf(d.ring),d.arc,this.arcs.indexOf(d.arc),d));

    }

    /**
     * Construct d3 arc.
     * @param {integer} i - angle index in circle
     * @param {boolean} isAnnotation - TRUE if angles are for clock hour annotations
     * @param {float} radiusInner - angle inner radius value
     * @param {float} radiusOuter - angle outer radius value
     * @returns An object with key/values representing a d3.js arc.
     */
    constructAngle(i, radiusInner, radiusOuter, isAnnotation=false) {
        return {
            startAngle: degreeToRadian((i * this.degree) + this.rotation),
            endAngle: degreeToRadian((i * this.degree) + this.degree + this.rotation),
            innerRadius: isAnnotation ? (this.radius - this.width) : radiusInner,
            outerRadius: isAnnotation ? (this.radius - this.width) : radiusOuter
        };
    }

    /**
     * Construct svg arc values.
     * @param {integer} arcIndex - arc index in ring
     * @param {integer} ringIndex - ring index in set of concentric cirlces
     * @returns An array of objects where each represents an arc in the radial clock grid.
     */
    constructArc(ringIndex,arcIndex) {

        // determine upper bounds of arc radius
        let outerRadius = this.ringRadius(ringIndex);
        
        // declare arc properties
        // inner radius seems backward; + vs. - bc of svg artboard direction
        return this.constructAngle(arcIndex, outerRadius + this.ring, outerRadius);

    }

    /**
     * Generate svg paths for arcs divided into even slices for a specified ring.
     * @param {string} ring - label for ring
     * @param {integer} ringIndex - ring index in set of concentric cirlces
     * @returns An array of objects where each represents an arc in the radial clock grid.
     */
    constructArcGrid(ring,ringIndex) {
        return this.arcs.map((a,i) => this.constructDatum(ring,ringIndex,a,i,i));
    }

    /**
     * Build data object for render.
     * @param {string} arcLabel - label for arc
     * @param {integer} arcIndex - arc index in ring
     * @param {object} d - original datum
     * @param {string} ringLabel - label for ring
     * @param {integer} ringIndex - ring index in set of concentric cirlces
     * @returns An array of objects where each represents an arc in the radial clock grid.
     */
    constructDatum(ringLabel,ringIndex,arcLabel,arcIndex,d) {

        // build arc
        let _arc = this.constructArc(ringIndex,arcIndex);

        // generate arc path/centroid
        return {
            arc: arcLabel,
            centroid: arc().centroid(_arc),
            degree: radianToDegree(_arc.startAngle) + (this.degree / 2),
            id: d.id,
            label: `${d.arc || arcLabel} ${d.ring || ringLabel}`,
            path: arc()(_arc),
            ring: ringLabel,
            value: d[this.valueParse] || d.value
        };

    }

}

export { ClockLayout };
export default ClockLayout;