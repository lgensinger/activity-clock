# Activity Clock

ES6 d3.js activity clock visualization.

## Install

```bash
# install package
npm install @lgv/activity-clock
```

## Data Format

The following values are the expected input data structure.

```json
[
    {
        "timestamp": "2021-07-31T16:05:55-04",
        "value": 1
    },
    {
        "timestamp": "2021-07-01T18:05:55-04",
        "value": 3
    },
    {
        "timestamp": "2021-07-30T16:10:55-04",
        "value": 2
    },
    {
        "timestamp": "2022-09-02T02:10:55-04",
        "value": 5
    }
]
```

## Use Module

```bash
import { ActivityClock } from "@lgv/activity-clock";

// have some data
let data = [
    {
        "timestamp": "2021-07-31T16:05:55-04",
        "value": 1
    },
    {
        "timestamp": "2021-07-01T18:05:55-04",
        "value": 3
    },
    {
        "timestamp": "2021-07-30T16:10:55-04",
        "value": 2
    },
    {
        "timestamp": "2022-09-02T02:10:55-04",
        "value": 5
    }
]

// initialize
const ac = new ActivityClock(data);

// render visualization
ac.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-activity-clock` | top-level svg element |
| `lgv-arc` | arc element |
| `lgv-content` | content margined from artboard |
| `lgv-label` | arc value label element |
| `lgv-label-arc` | arc label element |
| `lgv-label-ring` | ring label element |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd activity-clock

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run example

# view visualization in browser at http://localhost:8080
```
