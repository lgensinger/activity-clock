import moment from "moment";

import packageJson from "../package.json";
import { ActivityClock, ClockLayout } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

function rando() { return Math.floor(Math.random() * 1000); }

let data = [...Array(200).keys()].map(d => { 
    let start = moment().utc();
    return {
        time_end: start.add(rando(),"hour").format(),
        time_start: start.format(),
        value: rando() 
        }
    });

// get elements
let container = document.getElementsByTagName("figure")[0];
let c2 = document.getElementsByTagName("figure")[1];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // determine configs
    let width = container.offsetWidth;
    let height = width*0.75;

    // initialize
    const ac = new ActivityClock(data,width,height);

    // render visualization
    ac.render(container);

}

// load document
document.onload = startup(data,container);

// attach events
container.outputContainer = outputContainer;
container.addEventListener("arc-click",processEvent);
container.addEventListener("arc-mouseover", processEvent);
container.addEventListener("arc-mouseout", processEvent);